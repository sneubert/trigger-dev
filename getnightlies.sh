#!/bin/bash
set -e

PROJECTS=$(echo ${1} | tr " " ,)
SLOT=${2}
DAY=${3}
TARGET=${4}

echo "Downloading ${PROJECTS} and dependencies from ${SLOT}/${DAY} ..."
TMP=`mktemp -d`
lbn-install --insecure --dest ${TMP} --projects=${PROJECTS} \
  --platforms=${CMTCONFIG} ${SLOT} ${DAY}
for path in ${TMP}/*/; do
  PROJ=$(basename ${path})
  Proj=$(python -c "import json; print {p['name'].upper():p['name'] for p in json.load(open('${TMP}/slot-config.json'))['projects']}['${PROJ}']")
  if [[ ( $Proj == DBASE* ) || ( $Proj == PARAM* ) ]] ; then
    continue
  fi
  echo "Installing ${Proj}..."
  rm -rf ${Proj}
  cp -r ${path}* ${Proj}
  echo ${Proj} >> ${TMP}/.nightlies
done
cp ${TMP}/slot-config.json .slot-config.json
cp ${TMP}/.nightlies ${TARGET}
rm -rf ${TMP}

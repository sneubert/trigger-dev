# Tools for trigger development

This project contains a handful of tools and scripts that allow easy checkout
and build of the Hlt and Moore projects.
It is meant to replace the lb-dev workflow, which often leads to problems and
is not meant for longer term or collaborative developments.

This project was forked from the upgrade hackathon setup project:
https://gitlab.cern.ch/lhcb/upgrade-hackathon-setup


## Running on lxplus
Make sure you have enough space (1.5 GB) in the working area and you are
logged in to an `lxplus7` machine.

First, setup the environment and clone the repository
```
source /cvmfs/lhcb.cern.ch/group_login.sh -c x86_64-centos7-gcc62-opt  # use CVMFS with the correct gcc version
cd /afs/cern.ch/work/X/USERNAME/

git clone ssh://git@gitlab.cern.ch:7999/lhcb-HLT/trigger-dev.git workspace
cd workspace
```

By default, the `Hlt` and `Moore` projects will be cloned from git and
their dependencies will be taken from the last `lhcb-2018-patches` nightly.
If you prefer to use a different nightly, or the released versions
(which might not be compatible if the stack was released some time ago),
you need to edit `configuration.mk`.

Now you are ready to download, checkout and build everything with
```
export LC_ALL=C
make
```

Every time you modify something in e.g. Hlt, you need to recompile.
To run some options in the Moore environment, do
```
make Moore
Moore/run gaudirun.py my_options.py
```

Note that sometimes on lxplus builds with ninja are killed because they use too
much the machine (you would get an _internal compiler error_ from gcc).
Just run `make` again. Also make sure the master build (`make` or `make Moore`)
run with just one make thread (`-j1`); the builds for each project are already
multithreaded, and dependencies are (currently) not picked up correctly if the
master `make` process is multithreaded.


## Running elsewhere
You can also run on any (virtual) machine that has CVMFS installed.


## Committing and pushing changes
First, make sure the work you are about to commit does not break something.
The best way is to run the `2018_pp` test with
```
make Moore/test ARGS='-V -R 2018_pp$$'
```

Then, enter the Hlt project directory and make a branch if not done already.
Give branches descriptive names, e.g. `LBHLT-111` (corresponding JIRA task) or
`fix-b2hh-mass-cut`. Prepending your username is also a good idea.
```
cd Hlt
git checkout -b rmatev-LBHLT-111  # create and checkout a new branch
```

Stage the files and commit
```
git add ...
git commit ...
```

Once happy, push to GitLab
```
# first time
git push -u origin rmatev-LBHLT-111
# from then on
git push
```

The great thing about this approach is that it is simple.
You have a clone of the (full) Hlt project, and any git workflow should work as
expected, e.g. resolving conflicts with merge or rebase, taking some changes that
a colleague committed to your branch, etc.


## Makefile instructions
The `Makefile` provided features a few useful targets:

* basic commands
  * _all_ (or _build_): builds all the projects (this is the default)
  * _checkout_: get the sources
  * _update_: update the projects sources
  * _clean_: run a clean of all packages (keeping the sources)
  * _purge_: similar to _clean_, but remove the cmake temporary files
  * _deep-purge_: similar to _clean_, but remove the sources too
  * _help_: print a list of available targets
  * _use-git-xyz_: change the Git remote url of the checkout out projects,
    with _xyz_ replaces with any of _https_, _ssh_ or _krb5_
  * _for-each CMD="do-something"_: run a command in each project directory
* nightlies
  * _nightlies-update_: update to the latest nightlies
  * _nightlies-purge_: remove the installed projects from the nightlies
* helpers
  * _pull-build_: get a prebuilt image of all the projects
* special project targets
  * _\<Project\>_: build the required project (with dependencies),
  * _\<Project\>/\<target\>_: build the specified target in the given project,
    for example, to get the list of targets available in Moore you can call
    `make Moore/help`
  * _\<Project\>-\<action\>_: where _\<action\>_ can be _checkout_, _update_,
    _clean_ or _purge_, triggers the action on the specific project (with
    dependencies where it applies)
* _fast_ targets are available for targets with dependencies, for example
  * _fast/\<Project\>_: same as the target _\<Project\>_, but do not try to
    build the dependencies


## Running standard tests
LHCb projects come with several tests that can be run via the standard `ctest`
command from the project build directories
(e.g. `Moore/build.$CMTCONFIG`), or via the some helper targets in
the top level Makefile, for example:
```
make Moore/test ARGS='-N'
make Moore/test ARGS='-V -R 2018_pp$$'
```
where the content of the `ARGS` variable is passed to the `ctest` command line.

The arguments that can be passed to `ctest` can be found with `ctest --help`.
Some examples:

* `-N`: just print the names of the tests
* `-R <regex>`: select tests by regular expression
* `-V`: verbose printout (extra details, command line)


Tests hide the output of the job while it's run, but you can find the `.qmt`
file used for the test and run it through `gaudirun.py`, for example:
```
make Moore/test ARGS="-N -V -R 2018_pp$$"
Moore/run gaudirun.py Moore/Hlt/Moore/tests/qmtest/moore.qms/physics.qms/2018_pp.qmt
```

## Debugging
The projects built here are available only as optimized builds, so some special
actions are needed to be able to debug pieces of code:

* if you are using Docker, make sure you started `lb-docker-run` with the option
  `--privileged`
* add the good version of gdb to the path
  ```
  export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/gdb/7.11/x86_64-slc6-gcc49-opt/bin:$PATH
  ```
* change the configuration of the project containing the code to debug
  ```
  sed -i 's/CMAKE_BUILD_TYPE:STRING=.*/CMAKE_BUILD_TYPE:STRING=Debug/' Project/build.${CMTCONFIG}/CMakeCache.txt
  ```
* rebuild the project
  ```
  make Project
  ```
* run the job through the debugger
  ```
  Project/run gdb --args python $(Project/run which gaudirun.py) my_options.py
  ```
